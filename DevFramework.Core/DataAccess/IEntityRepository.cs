﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

using DevFramework.Core.Entities;

namespace DevFramework.Core.DataAccess
{
    //Generic 
    public interface IEntityRepository<T> where T:class, IEntity, new()
    {
        //Operations
        //Get all data if Filters avaliable or not
        //If filter null return all list.
        List<T> GetList(Expression<Func<T,bool>> filter=null );

        //Single element
        T Get(Expression<Func<T, bool>> filter = null);

        //Add Operation
        T Add(T entity);

        //Update Operation
        T Update(T entity);

        //Delete Operation
        void Delete(T entity);
    }
}
