﻿using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//For lazy loading
namespace DevFramework.Core.DataAccess.NHibernat
{
    public abstract  class  NHibernateHelper : IDisposable
    {
        //Like context in entity framework
        static ISessionFactory _sessionFactory;

        //Session Solution structure.
        public virtual ISessionFactory SessionFactory
        {
            //If sessionfactory is avaliable use it or use inital factory which we write
            get { return _sessionFactory ?? (_sessionFactory = InitilizeFactory()); }
        }

        //it changes upon the environment (SQL/ ORACLE)
        protected abstract ISessionFactory InitilizeFactory();
       
        public virtual ISession OpenSession()
        {
            return SessionFactory.OpenSession();
        }
        public void Dispose()
        {

            GC.SuppressFinalize(this);
        }

        
        

    }
}
