﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevFramework.Northwind.DataAccess.Concrete.EntityFramework.Mappings;
using DevFramework.Northwind.Entities.Concrete;

namespace DevFramework.Northwind.DataAccess.Concrete.EntityFramework
{
   public class NorthwindContext:DbContext
    {
        //NOTE: USING already made Database close the migration
        public NorthwindContext()
        {
            Database.SetInitializer<NorthwindContext>(null);
        }

        public DbSet<Product> Products { get; set; }

        //Implement othe Dbset bellow


        //Product mapping ovveride on creating method
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //add your mappings here
            modelBuilder.Configurations.Add(new ProductMap());
        }
    }
}
