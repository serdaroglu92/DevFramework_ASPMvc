﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using DevFramework.Core.DataAccess.NHibernat;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;

namespace DevFramework.Northwind.DataAccess.Concrete.NHibernate.Helpers
{
    public class SqlServerHelper:NHibernateHelper
    {
        protected override ISessionFactory InitilizeFactory()
        {
            //orcale .net configuartion
            // return Fluently.Configure().Database(OracleClientConfiguration.Oracle10);
            //in place of Northwind Context you can add connection string to db.

            return Fluently.Configure().Database(MsSqlConfiguration.MsSql2012
                .ConnectionString(c => c.FromConnectionStringWithKey("NorthwindContext")))
                .Mappings(t => t.FluentMappings.AddFromAssembly(assembly: Assembly.GetExecutingAssembly()))
                .BuildSessionFactory();
        }
    }
}
