﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevFramework.Northwind.Entities.Concrete;
using FluentNHibernate.Mapping;

namespace DevFramework.Northwind.DataAccess.Concrete.NHibernate.Mappings
{
   public  class ProductMap:ClassMap<Product>
    {
        //with help of constructor we specifiy this object will go to which table
        public ProductMap()
        {
            Table(@"Products");

            //Enable lazy loading
            LazyLoad();

            Id(x => x.ProductId).Column("ProductId");

            Map(x => x.CategoryId).Column("CategoryID");
            Map(x => x.ProductName).Column("ProductName");
            Map(x => x.QuantityPerUnit).Column("QuantityPerUnit");
            Map(x => x.UnitPrice).Column("UnitPrice");

        }
    }
}
